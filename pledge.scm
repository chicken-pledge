(declare
  (module (pledge))
  (import (chicken foreign))
  (disable-interrupts)
  (fixnum-arithmetic)
  (export pledge))

(foreign-declare "#include <unistd.h>")

(define pledge
  (let ((f (foreign-lambda int pledge (const c-string) (const c-string))))
    (lambda (promises #!optional (execpromises #f))
      (f promises execpromises))))
