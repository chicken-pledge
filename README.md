chicken-pledge
==============
CHICKEN bindings to OpenBSD's [`pledge(2)`][1] system call.

Usage
-----
A single procedure called `pledge` is provided, which has the same
interface as the system call. On error, `-1` is returned and `errno`
should be consulted.

    (use pledge)
    (pledge "rpath stdio unix")

License
-------
3-clause BSD. See LICENSE for details.

[1]: http://man.openbsd.org/OpenBSD-current/man2/pledge.2
